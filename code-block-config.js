
module.exports = [{
  name: "内嵌表单的弹窗",
  value: {
    depo: "https://gitee.com/bottom_level_aided_rd/code-blocks",
    remoteFile: "FormModalDemo.js"
  }
}, {
  name: "(私有)表格代码模板",
  value: {
    depo: "https://gitee.com/bottom_level_aided_rd/code-blocks",
    remoteDirectory: "TableTemplate"
  }
}, {
  name: "(公有)表格代码模板",
  value: {
    depo: "https://gitee.com/bottom_level_aided_rd/code-blocks",
    remoteDirectory: "Public-TableTemplate"
  }
}, {
  name: "查询块模板",
  value: {
    depo: "https://gitee.com/bottom_level_aided_rd/code-blocks",
    remoteDirectory: "QueryBlock"
  }
}, {
  name: "动态表单项",
  value: {
    depo: "https://gitee.com/bottom_level_aided_rd/code-blocks",
    remoteDirectory: "DynamicFormList"
  }
}, {
  name: "弹窗管理",
  value: {
    depo: "https://gitee.com/bottom_level_aided_rd/code-blocks",
    remoteDirectory: "withFloatLayer"
  }
}, {
  name: "网络请求",
  value: {
    depo: "https://gitee.com/bottom_level_aided_rd/code-blocks",
    remoteDirectory: "$request"
  }
}, {
  name: "异步常数",
  value: {
    depo: "https://gitee.com/bottom_level_aided_rd/code-blocks",
    remoteFile: "AsyncConstant.js"
  }
}, {
  name: "环境断言",
  value: {
    depo: "https://gitee.com/bottom_level_aided_rd/code-blocks",
    remoteFile: "ENV_ASSERT.js"
  }
}]