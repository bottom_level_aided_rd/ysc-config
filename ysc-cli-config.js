

// lib是类库型的脚手架
exports.lib=[{
  name: "[-]只通过gitee进行发布的包",
  value: {
    defaultName: "js-github-lib",
    giturl: "https://github.com/ysc-template/js-github-lib-template.git"
  }
},{
  name:"[-]创建crgt内部pc端js脚手架",
  value:{
    defaultName:"ts-lib-client-pc",
    giturl:"https://github.com/ysc-template/ts-lib-client-pc.git"
  }
}];


// pro是项目型的脚手架
exports.pro=[ {
  name: "[-]创建antd-pc工程脚手架",
  value: {
    defaultName: "my-react-ant-demo",
    giturl: "https://github.com/ysc-template/react-ant-template.git"
  }
}, {
  name: "[-]创建antd-mobile工程脚手架",
  value: {
    defaultName: "my-react-ant-mobile",
    giturl: "https://github.com/ysc-template/react-ant-mobile-template.git"
  }
},{
  name: "[-]创建普通的react工程脚手架",
  value: {
    defaultName: "my-react-spa",
    giturl: "https://github.com/ysc-template/react-spa-template.git"
  }
}]

// other是其他类型的脚手架
exports.other=[{
  name: "[-]未分类的脚手架",
  value: {
    defaultName: "my-react-ant-demo",
    giturl: "https://github.com/ysc-template/react-ant-template.git"
  }
}]